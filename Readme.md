# [marley.org](https://marley.org) source codes

<br/>

### Run marley.org on localhost

    # vi /etc/systemd/system/marley.org.service

Insert code from marley.org.service

    # systemctl enable marley.org.service
    # systemctl start  marley.org.service
    # systemctl status marley.org.service

http://localhost:4029
